﻿using System.Collections.Generic;
using System.Linq;

namespace ConstructionLine.CodingChallenge
{
    public class SearchEngine
    {
        private readonly List<Shirt> _shirts;

        private readonly Dictionary<string, ColorCount> _countsByColor = new Dictionary<string, ColorCount>();
        private readonly Dictionary<string, SizeCount> _countsBySize = new Dictionary<string, SizeCount>();
        private readonly Dictionary<string, IList<Shirt>> _shirtIdsByColor = new Dictionary<string, IList<Shirt>>();

        public SearchEngine(List<Shirt> shirts)
        {
            _shirts = shirts;

            InitializeSearchData();
            foreach (var shirt in shirts)
            {
                _shirtIdsByColor[shirt.Color.Name].Add(shirt);
            }
        }

        public SearchResults Search(SearchOptions options)
        {
            var searchResults = InitSearchResults();

            foreach (var color in _shirtIdsByColor)
            {
                if (!options.Colors.Any() || options.Colors.Any(c => c.Name == color.Key))
                {
                    var additionalShirts = color.Value.ToList()
                        .Where(s => !options.Sizes.Any() || options.Sizes.Any(c => s.Name == s.Size.Name));

                    searchResults.Shirts.AddRange(additionalShirts);
                }
            }

            PopulateResultCounts(searchResults);

            return searchResults;
        }

        private void PopulateResultCounts(SearchResults searchResults)
        {
            foreach (var shirt in searchResults.Shirts)
            {
                _countsByColor[shirt.Color.Name].Count++;
                _countsBySize[shirt.Size.Name].Count++;
            }

            searchResults.ColorCounts = _countsByColor.Values.ToList();
            searchResults.SizeCounts = _countsBySize.Values.ToList();
        }

        private void InitializeSearchData()
        {
            foreach (var color in Color.All)
            {
                _shirtIdsByColor.Add(color.Name, new List<Shirt>());
            }
        }

        private SearchResults InitSearchResults()
        {
            foreach (var size in Size.All)
            {
                _countsBySize.Add(size.Name, new SizeCount {Size = size, Count = 0});
            }

            foreach (var color in Color.All)
            {
                _countsByColor.Add(color.Name, new ColorCount() { Color = color, Count = 0 });
            }
            return new SearchResults
            {
                Shirts = new List<Shirt>()
            };
        }
    }
}